import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookAppointmentRoutingModule } from './book-appointment-routing.module';
import { BookComponentComponent } from './book-component/book-component.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    //BookComponentComponent
  ],
  imports: [
    CommonModule,
    BookAppointmentRoutingModule,
    ReactiveFormsModule
  ]
})
export class BookAppointmentModule { }
