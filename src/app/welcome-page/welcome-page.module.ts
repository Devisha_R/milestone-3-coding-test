import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelcomePageRoutingModule } from './welcome-page-routing.module';
import { LandingComponentComponent } from './landing-component/landing-component.component';


@NgModule({
  declarations: [
    //LandingComponentComponent
  ],
  imports: [
    CommonModule,
    WelcomePageRoutingModule
  ]
})
export class WelcomePageModule { }
