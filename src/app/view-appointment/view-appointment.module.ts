import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewAppointmentRoutingModule } from './view-appointment-routing.module';

import { ViewComponentComponent } from './view-component/view-component.component';


@NgModule({
  declarations: [
    
    //ViewComponentComponent
  ],
  imports: [
    CommonModule,
    ViewAppointmentRoutingModule
  ]
})
export class ViewAppointmentModule { }
