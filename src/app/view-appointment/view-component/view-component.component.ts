import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services.service';

@Component({
  selector: 'app-view-component',
  templateUrl: './view-component.component.html',
  styleUrls: ['./view-component.component.scss']
})
export class ViewComponentComponent implements OnInit {

  constructor(private apiservice:ServicesService) { }
   data:any;
  ngOnInit(): void {
    this.apiservice.get().subscribe(response=>{
      this.data = response;
      console.log(this.data);
      console.log(typeof this.data)

    }

      )
  }

}
