import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookComponentComponent } from './book-appointment/book-component/book-component.component';
import { HealthclubInterceptor } from './healthclub.interceptor';
import { QueryComponentComponent } from './query/query-component/query-component.component';
import { ViewComponentComponent } from './view-appointment/view-component/view-component.component';
import { LandingComponentComponent } from './welcome-page/landing-component/landing-component.component';

@NgModule({
  declarations: [
    AppComponent,
    ViewComponentComponent,
    BookComponentComponent,
    QueryComponentComponent,
    LandingComponentComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    
    
    
    
    
  ],
  providers: [
    {provide:HTTP_INTERCEPTORS,useClass:HealthclubInterceptor,multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

