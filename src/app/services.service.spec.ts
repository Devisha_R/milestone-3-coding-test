import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { ServicesService } from './services.service';

describe('ServicesService', () => {
  let service: ServicesService;

  beforeEach(() => {
    
    TestBed.configureTestingModule({
      imports:[

        RouterTestingModule,

        FormsModule,

        HttpClientModule,

        ReactiveFormsModule,

        //ToastrModule.forRoot()

      ],

      providers:[

        HttpClient

      ]
    });
    
    service = TestBed.inject(ServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
