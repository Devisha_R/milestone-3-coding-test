import { TestBed } from '@angular/core/testing';

import { HealthclubInterceptor } from './healthclub.interceptor';

describe('HealthclubInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HealthclubInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: HealthclubInterceptor = TestBed.inject(HealthclubInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
