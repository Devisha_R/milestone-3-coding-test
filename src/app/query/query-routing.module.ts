import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QueryComponentComponent } from './query-component/query-component.component';

const routes: Routes = [
  {
    path: 'query',
    component: QueryComponentComponent,}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QueryRoutingModule { }
