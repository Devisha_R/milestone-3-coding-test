import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { QueryComponentComponent } from './query-component.component';

describe('QueryComponentComponent', () => {
  let component: QueryComponentComponent;
  let fixture: ComponentFixture<QueryComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        HttpClientModule
      ],

      declarations: [ QueryComponentComponent ],
      providers: [
        HttpClient
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QueryComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
