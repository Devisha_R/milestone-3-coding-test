import { Injectable } from '@angular/core';
import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  BaseUrl : string ="http://localhost:3000/health-member";
  constructor(private http: HttpClient) { }
   get(){
    return this.http.get(this.BaseUrl);

  }
  post(appointment:any){
    return this.http.post(this.BaseUrl,appointment);
  }
}
