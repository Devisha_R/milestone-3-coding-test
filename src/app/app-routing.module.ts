import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookComponentComponent } from './book-appointment/book-component/book-component.component';
import { QueryComponentComponent } from './query/query-component/query-component.component';
import { ViewComponentComponent } from './view-appointment/view-component/view-component.component';
import { LandingComponentComponent } from './welcome-page/landing-component/landing-component.component';

const routes: Routes = [
  {
    path:'landing-page',
    component:LandingComponentComponent,
   },
   {path: 'view-component',
   component: ViewComponentComponent,},

   {path: 'book-component',
   component: BookComponentComponent,},

   {path: 'query',
    component: QueryComponentComponent,}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
